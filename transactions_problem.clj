
;; Seller reports

(def alltransactions (read-string (slurp "C:\\Users\\mallesi\\Desktop\\transactions.txt")))

(def alltransactionsBySeller (group-by :seller-id alltransactions))

(defn update-vals [map vals f] (reduce #(update-in % [%2] f) map vals))

(def biggestSaleBySeller (update-vals alltransactionsBySeller (keys alltransactionsBySeller) #(apply max (map :amount %)) ) )

(def totalEarnedBySeller (update-vals alltransactionsBySeller (keys alltransactionsBySeller) #(apply + (map :amount %)) ))

(def transactionsBySellerAndBuyer (update-vals alltransactionsBySeller (keys alltransactionsBySeller) #(group-by :buyer-id %) ) )
(def totalSpentByEachBuyer (update-vals transactionsBySellerAndBuyer (keys transactionsBySellerAndBuyer) (fn [sellerData] (update-vals sellerData (keys sellerData) #(apply + (map :amount %) ) ) ) ) )

(def bestCustomerForSeller (update-vals totalSpentByEachBuyer (keys totalSpentByEachBuyer) #(apply max-key val %) ))

(doall (map #(spit (str "seller_" (str (key %) ".txt")) (str "Total Amount Earned:" (val %) "\n") :append true) totalEarnedBySeller))
(doall (map #(spit (str "seller_" (str (key %) ".txt")) (str "Biggest sale:" (val %) "\n") :append true ) biggestSaleBySeller))
(doall (map #(spit (str "seller_" (str (key %) ".txt")) (str "Best Customer:" (get (val %) 0) "\n") :append true) bestCustomerForSeller))


;;Buyer Reports

(def allTransactionsByBuyer (group-by :buyer-id alltransactions))

;; Biggest purchase:

 (def biggestPurchaseForBuyer (update-vals allTransactionsByBuyer (keys allTransactionsByBuyer) #(apply max (map :amount %)) ) )
 (doall (map #(spit (str "buyer_" (str (key %) ".txt")) (str "Biggest Purchase:" (val %) "\n") :append true) biggestPurchaseForBuyer))
;; Total amount spent

 (def totalSpentByEachBuyer (update-vals allTransactionsByBuyer (keys allTransactionsByBuyer) #(apply + (map :amount %)) ))

 (doall (map #(spit (str "buyer_" (str (key %) ".txt")) (str "Total Amount Spent:" (val %) "\n") :append true) totalSpentByEachBuyer))

;; Favorite Business (by number of purchases)

(def transactionsByBuyerAndSeller (update-vals allTransactionsByBuyer (keys allTransactionsByBuyer) #(group-by :seller-id %) ) )
(def numberOfPurchases (update-vals transactionsByBuyerAndSeller (keys transactionsByBuyerAndSeller) (fn [buyerData] (update-vals buyerData (keys buyerData) #(count %) ) ) ) )
(def favoriteBusiness (update-vals numberOfPurchases (keys numberOfPurchases) #(apply max-key val %) ) )
(doall (map #(spit (str "buyer_" (str (key %) ".txt")) (str "Favorite Business:" (get (val %) 0) "\n") :append true) favoriteBusiness))
